# Backend Challenge

API que testa a validade de um input dado às regras:

### Regras

- Nove ou mais caracteres
- Ao menos 1 dígito
- Ao menos 1 letra minúscula
- Ao menos 1 letra maiúscula
- Ao menos 1 caractere especial
- - Considere como especial os seguintes caracteres: !@#$%^&\*()-+
- Não possuir caracteres repetidos dentro do conjunto

**Exemplo:**

```
IsValid("") // false
IsValid("aa") // false
IsValid("ab") // false
IsValid("AAAbbbCc") // false
IsValid("AbTp9!foo") // false
IsValid("AbTp9!foA") // false
IsValid("AbTp9 fok") // false
IsValid("AbTp9!fok") // true
```

## Ferramentas utilizadas:

- Javascript, [Typescript](https://www.typescriptlang.org/) e [Node.js](https://nodejs.org/)

## Pré-requisitos

- Node.js versão 14.17.0 e NPM 6.14.13

A forma mais fácil de instalar é com o [nvm](https://github.com/nvm-sh/nvm#installing-and-updating)

## Executando o projeto

Utilizando a versão 14.17.0 do Node.js e 6.14.13 do NPM

```
npm i
npm run dev
```

A seguinte informação deverá aparecer no terminal:

```
[INFO] XX:XX:XX ts-node-dev ver. 1.1.6 (using ts-node ver. 9.1.1, typescript ver. 4.2.4)
Server running at: http://localhost:${PORT}
```

A porta padrão é a 5000, mas é possível mudar criando um arquivo .env e declarando:

```
PORT=XXXX
```

Assim será possível fazer um POST request para o endpoint:

```
http://localhost:5000/api/validate-password/
```

utilizando sua ferramenta favorita.

- O projeto foi testado somente no macOS Big Sur

## Lint

A ferramenta utilizada é o [Eslint](https://eslint.org/), utilizando a configuração padrão.

```
npm i
npm run lint
```

Não deverá aparecer nenhum erro no terminal.

### Executando os testes

Os testes de integração e Unitários foram feitos com o [Jest](https://jestjs.io/):

```
npm i
npm test
```

A seguinte informação deverá aparecer no terminal:

```
Test Suites: 9 passed, 9 total
Tests:       24 passed, 24 total
Snapshots:   0 total
Time:        2.004 s
Ran all test suites.
```

### Testando manualmente

Enviando uma string que não atende as regras:

```
curl --location --request POST 'http://localhost:5000/api/validate-password/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "password": "fweewffewfewfew"
}'
```

o retorno será:

```
{"isValid":false}%
```

Já para uma string atendendo as regras:

```
curl --location --request POST 'http://localhost:5000/api/validate-password/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "password": "AbTp9!fok"
}'
```

o retorno será:

```
{"isValid":true}%
```

Já uma chamada com input inválido:

```
curl --location --request POST 'http://localhost:5000/api/validate-password/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "notpassword": "AbTp9!fok"
}'
```

retornará:

```
{"target":{"password":""},"value":"","property":"password","children":[],"constraints":{"isNotEmpty":"password should not be empty"}}%
```

### Arquitetura

A arquitetura utilizada foi a [Clean Architeture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html), utilizando 4 camadas:

- domain - Onde são definidas as regras de negócio
- data - Implementação das regras
- infra - Infraestrutura necessária para a implementação das regras
- presentation - Apresentação dos dados para o usuário final

A vantagem é o grande desacoplamento feito, se utilizando da inversão de dependência, podendo separar as regras de negócio de sua implementação, facilitando mudanças e extensões do código.

### Premissas

#### Espaços em branco não devem ser considerados como caracteres válidos

Escolhi implementar uma regra, que invalida qualquer input com espaços em branco. Fiquei em dúvida se era só para ignorar, mas parecia fazer mais sentido já barrar qualquer senha do tipo, já que não é de comum a utilização pelos usuários.

#### Não possuir caracteres repetidos dentro do conjunto

Escolhi implementar a regra que já invalida qualquer senha com caractere repetido. Fiquei em dúvida se os caracteres deveriam ser adjacentes ou não.

### Detalhes

- Decidi permitir a fácil mudança do número de caracteres nas regras, também poderia ter adicionado regras opostas, ex: (máximo de dígitos ao invés de mínimo), mas achei que uma regra assim não seria muito usual para validação de senhas.
- Deixei a validação do retorno do input feita pela library [Class Validator](https://github.com/typestack/class-validator), mas seria possível padronizar os erros dependendo dos requisitos do projeto.
- Os erros da validação da senha não são exibidos pois não constava na resposta pedida pelo desafio, mas já são calculados e seria facilmente possível retorná-los para facilitar a validação da senha pelo usuário.
