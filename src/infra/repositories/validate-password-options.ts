import { ValidatePasswordOptions } from '@server/data/services'

export const defaultValidatePasswordOptions: ValidatePasswordOptions = {
  minNumberOfCharacters: 9,
  minNumberOfDigits: 1,
  minNumberOfLowerCaseLetters: 1,
  minNumberOfSpecialCharacters: 1,
  minNumberOfUpperCaseLetters: 1
}
