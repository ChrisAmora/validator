import 'dotenv/config'
import app from '@server/main/config/app'
import { env } from '@server/main/config/env'


app.listen(env.port, () => console.log(`Server running at: http://localhost:${env.port}`))
