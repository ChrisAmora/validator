import { adaptRoute } from '@server/main/adapters'
import { makeValidatePasswordController } from '@server/main/factories'
import { Router } from 'express'

export default (router: Router): void => {
  router.use((req, res, next) => {
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (!req.is('application/json')) {
      res.sendStatus(400)
    } else {
      next()
    }
  })
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  router.post('/validate-password', adaptRoute(makeValidatePasswordController()))
}
