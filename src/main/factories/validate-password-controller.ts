import { ValidatePasswordService } from '@server/data/services'
import { defaultValidatePasswordOptions } from '@server/infra/repositories'
import { ValidatePasswordController } from '@server/presentation/controllers'
import { Controller } from '@server/presentation/interfaces'

export const makeValidatePasswordController = (): Controller => {
  const options = defaultValidatePasswordOptions
  const validator = new ValidatePasswordService(options)
  return new ValidatePasswordController(validator)
}
