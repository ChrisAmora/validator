import { ValidatePassword } from '@server/domain/usecases'
import { badRequestError, ok, serverError } from '@server/presentation/controllers/response'
import { Controller, HttpRequest, HttpResponse } from '@server/presentation/interfaces'
import { PasswordValidation, validateInput } from '@server/presentation/validation'
import { mapToViewModel, ValidatePasswordViewModel } from '@server/presentation/view-models'

export class ValidatePasswordController implements Controller {
  constructor(private readonly validatePassword: ValidatePassword) {}

  async handle(httpRequest: HttpRequest<any, { password: string }>): Promise<HttpResponse<ValidatePasswordViewModel>> {
    const password = httpRequest?.body?.password ?? ''

    const inputToValidate = new PasswordValidation()
    inputToValidate.password = password
    const validatedInput = await validateInput(inputToValidate)
    if (validatedInput.length > 0) {
      return badRequestError(validatedInput?.[0])
    }

    try {
      const validation = await this.validatePassword.validate(password)
      return ok(mapToViewModel(validation))
    } catch (error) {
      return serverError(error)
    }
  }
}
