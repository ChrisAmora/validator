import { HttpResponse } from '@server/presentation/interfaces'

export const serverError = (error: Error): HttpResponse => ({
  statusCode: 500,
  data: error.stack
})

export const ok = <T = any>(data: T): HttpResponse<T> => ({
  statusCode: 200,
  data
})

export const badRequestError = <T = any>(data: T): HttpResponse => ({
  statusCode: 400,
  data
})
