import app from '@server/main/config/app'
import 'dotenv/config'
import request from 'supertest'

describe('Validate password Controller', () => {
  const apiUrl = '/api/validate-password'
  const tests: Record<string, boolean> = {
    aa: false,
    ab: false,
    AAAbbbCc: false,
    'AbTp9!foo': false,
    'AbTp9!foA': false,
    'AbTp9 fok': false,
    'AbTp9!fok': true
  }
  test('Should return the correct result', async () => {
    for (const key in tests) {
      const response = await request(app)
        .post(apiUrl)
        .send({ password: key })
        .set('Content-Type', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
      expect(response.body.isValid).toBe(tests[key])
    }
  })

  test('Should fail if content type is wrong', async () => {
    await request(app).post(apiUrl).set('Content-Type', 'text/html').expect(400)
  })

  test('Should fail if password is empty or not a string', async () => {
    await request(app).post(apiUrl).send({ test: 'eowfdkoef' }).expect(400)
    await request(app).post(apiUrl).send({ password: '' }).expect(400)
    await request(app).post(apiUrl).send({ password: 55 }).expect(400)
    await request(app).post(apiUrl).send({ password: true }).expect(400)
    await request(app)
      .post(apiUrl)
      .send({ password: { password: 'pass' } })
      .expect(400)
  })
})
