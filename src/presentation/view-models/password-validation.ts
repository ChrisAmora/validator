import { PasswordValidation } from '@server/domain'

export interface ValidatePasswordViewModel {
  isValid: boolean
}

export const mapToViewModel = (entity: PasswordValidation): ValidatePasswordViewModel => {
  return {
    isValid: entity.isApproved
  }
}
