export interface HttpResponse<T = any> {
  statusCode: number
  data: T
}

export interface HttpRequest<TParams = any, UBody = any> {
  body?: UBody
  headers?: any
  params?: TParams
}
