import { HttpRequest, HttpResponse } from '@server/presentation/interfaces'

export interface Controller {
  handle: (httpRequest: HttpRequest) => Promise<HttpResponse>
}
