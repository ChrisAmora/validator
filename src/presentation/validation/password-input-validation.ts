import { IsNotEmpty, IsString, validate, ValidationError } from 'class-validator'

export class PasswordValidation {
  @IsString()
  @IsNotEmpty()
  password!: string
}

export const validateInput = async (input: object): Promise<ValidationError[]> => {
  const validatedInput = await validate(input)
  return validatedInput
}
