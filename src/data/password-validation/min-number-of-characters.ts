import { BasePasswordValidationHandler, PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { ValidationError } from '@server/domain'

export class MinNumberOfCharactersHandler extends BasePasswordValidationHandler {
  private readonly minNumberOfCharacters: number

  constructor(minNumberOfCharacters: number) {
    super()
    this.minNumberOfCharacters = minNumberOfCharacters
  }

  handle(passwordValidation: PasswordValidationChain): PasswordValidationChain {
    const numberOfCharacters = passwordValidation.password.length
    if (numberOfCharacters < this.minNumberOfCharacters) {
      passwordValidation.errors.push(ValidationError.MinNumberOfCharacters)
    }

    return super.handle(passwordValidation)
  }
}
