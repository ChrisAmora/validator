import { PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { MinNumberOfUpperCaseLettersHandler } from '@server/data/password-validation/min-number-of-upper-case-letters'
import { ValidationError } from '@server/domain'

describe('Min Number Of Upper Case Letters Test', () => {
  test('Should fail if min of upper case letters is not reached', async () => {
    const handler = new MinNumberOfUpperCaseLettersHandler(3)
    const fail1 = handler.handle(new PasswordValidationChain('aa#AA92'))
    const fail2 = handler.handle(new PasswordValidationChain('lasagna'))
    const fail3 = handler.handle(new PasswordValidationChain(''))
    const fail4 = handler.handle(new PasswordValidationChain('23'))
    const fail5 = handler.handle(new PasswordValidationChain('EFablalalal#'))
    const fail6 = handler.handle(new PasswordValidationChain('###♥️♥️aa♥️♥️♥️55###'))

    expect(fail1.errors).toHaveLength(1)
    expect(fail1.errors[0]).toBe(ValidationError.MinNumberOfUpperCaseLetters)
    expect(fail2.errors).toHaveLength(1)
    expect(fail2.errors[0]).toBe(ValidationError.MinNumberOfUpperCaseLetters)
    expect(fail3.errors).toHaveLength(1)
    expect(fail3.errors[0]).toBe(ValidationError.MinNumberOfUpperCaseLetters)
    expect(fail4.errors).toHaveLength(1)
    expect(fail4.errors[0]).toBe(ValidationError.MinNumberOfUpperCaseLetters)
    expect(fail5.errors).toHaveLength(1)
    expect(fail5.errors[0]).toBe(ValidationError.MinNumberOfUpperCaseLetters)
    expect(fail6.errors).toHaveLength(1)
    expect(fail6.errors[0]).toBe(ValidationError.MinNumberOfUpperCaseLetters)
  })

  test('Should always pass if min is 0', async () => {
    const handler = new MinNumberOfUpperCaseLettersHandler(0)
    const success1 = handler.handle(new PasswordValidationChain('fewfefew'))
    const success2 = handler.handle(new PasswordValidationChain('AAPPQMM'))
    const success3 = handler.handle(new PasswordValidationChain(''))
    const success4 = handler.handle(new PasswordValidationChain('55'))
    const success5 = handler.handle(new PasswordValidationChain('efwefhuwfehuqooqjei@#$$  feufewuwfe #@!!@000 ♥️'))
    const success6 = handler.handle(new PasswordValidationChain('999999999999'))
    const success7 = handler.handle(new PasswordValidationChain('Á'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
    expect(success5.errors).toHaveLength(0)
    expect(success6.errors).toHaveLength(0)
    expect(success7.errors).toHaveLength(0)
  })

  test('Should pass if min is reached', async () => {
    const handler = new MinNumberOfUpperCaseLettersHandler(3)
    const success1 = handler.handle(new PasswordValidationChain('AMP'))
    const success2 = handler.handle(new PasswordValidationChain('♥️KPefijM'))
    const success3 = handler.handle(new PasswordValidationChain('a1AAcAA2A3b'))
    const success4 = handler.handle(new PasswordValidationChain('KFEOFWEOKaaa'))
    const success5 = handler.handle(new PasswordValidationChain('@##AA@@@aa@@@aZ'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
    expect(success5.errors).toHaveLength(0)
  })
})
