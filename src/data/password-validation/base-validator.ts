import { ValidationError } from '@server/domain'

interface CountCharcodeInRangeArgs {
  stringToValidate: string
  firstCharacter: string
  lastCharacter: string
}

const SPECIAL_CHARACTERS = '!@#$%^&*()-+'
export const specialCharactersMap = new Set(SPECIAL_CHARACTERS)

export const countCharcodeInRange = ({
  firstCharacter,
  lastCharacter,
  stringToValidate
}: CountCharcodeInRangeArgs): number => {
  let found = 0
  const firstCharacterCharcode = firstCharacter.charCodeAt(0)
  const lastCharacterCharcode = lastCharacter.charCodeAt(0)
  stringToValidate.split('').forEach((letter) => {
    const charCode = letter.charCodeAt(0)
    if (charCode >= firstCharacterCharcode && charCode <= lastCharacterCharcode) {
      found += 1
    }
  })
  return found
}

export class PasswordValidationChain {
  public password: string
  errors: ValidationError[] = []
  constructor(password: string) {
    this.password = password
  }
}

export abstract class BasePasswordValidationHandler {
  protected nextHandler: BasePasswordValidationHandler | null = null

  setNextHandler(handler: BasePasswordValidationHandler): BasePasswordValidationHandler {
    this.nextHandler = handler
    return handler
  }

  handle(passwordValidation: PasswordValidationChain): PasswordValidationChain {
    if (this.nextHandler != null) return this.nextHandler.handle(passwordValidation)
    return passwordValidation
  }
}
