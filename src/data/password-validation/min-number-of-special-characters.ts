import {
  BasePasswordValidationHandler,
  PasswordValidationChain,
  specialCharactersMap
} from '@server/data/password-validation/base-validator'
import { ValidationError } from '@server/domain'

export class MinNumberOfSpecialCharactersHandler extends BasePasswordValidationHandler {
  private readonly minNumberOfSpecialCharacters: number

  constructor(minNumberOfSpecialCharacters: number) {
    super()
    this.minNumberOfSpecialCharacters = minNumberOfSpecialCharacters
  }

  handle(passwordValidation: PasswordValidationChain): PasswordValidationChain {
    let specialCharactersFound = 0
    passwordValidation.password.split('').forEach((char) => {
      if (specialCharactersMap.has(char)) {
        specialCharactersFound += 1
      }
    })
    if (specialCharactersFound < this.minNumberOfSpecialCharacters) {
      passwordValidation.errors.push(ValidationError.MinNumberOfSpecialCharacters)
    }

    return super.handle(passwordValidation)
  }
}
