import { PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { HasNoBlankSpaceHandler } from '@server/data/password-validation/has-no-blank-space'
import { ValidationError } from '@server/domain'

describe('Has No Blank Space Test', () => {
  test('Should pass with all values that does not have an empty string', async () => {
    const handler = new HasNoBlankSpaceHandler()
    const normalString = handler.handle(new PasswordValidationChain('lasanha'))
    const largeString = handler.handle(new PasswordValidationChain('wefjeiwfjwfefamifeifjijmofwefowejnmqdopwjidijd'))
    const stringWithSpecialCharacters = handler.handle(new PasswordValidationChain('&蚟#$&&#(👏'))
    const stringWithDigits = handler.handle(new PasswordValidationChain('192929290109210'))
    const emptyString = handler.handle(new PasswordValidationChain(''))
    const HasBlankSpace = handler.handle(new PasswordValidationChain('fewfew fefe   ef'))

    expect(normalString.errors).toHaveLength(0)
    expect(largeString.errors).toHaveLength(0)
    expect(stringWithDigits.errors).toHaveLength(0)
    expect(stringWithSpecialCharacters.errors).toHaveLength(0)
    expect(emptyString.errors).toHaveLength(0)
    expect(HasBlankSpace.errors[0]).toBe(ValidationError.HasNoBlankSpace)
  })
})
