import { PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { NotRepeatedCharacterHandler } from '@server/data/password-validation/not-repeated-character'
import { ValidationError } from '@server/domain'

describe('Not Repeated Character Test', () => {
  test('Should fail if there is a repeadted character', async () => {
    const handler = new NotRepeatedCharacterHandler()
    const fail1 = handler.handle(new PasswordValidationChain('aa#AA92'))
    const fail2 = handler.handle(new PasswordValidationChain('fehuefhu1'))
    const fail3 = handler.handle(new PasswordValidationChain('♥️abc♥️'))
    const fail4 = handler.handle(new PasswordValidationChain('232'))

    expect(fail1.errors).toHaveLength(1)
    expect(fail1.errors[0]).toBe(ValidationError.NotRepeatedCharacters)
    expect(fail2.errors).toHaveLength(1)
    expect(fail2.errors[0]).toBe(ValidationError.NotRepeatedCharacters)
    expect(fail3.errors).toHaveLength(1)
    expect(fail3.errors[0]).toBe(ValidationError.NotRepeatedCharacters)
    expect(fail4.errors).toHaveLength(1)
    expect(fail4.errors[0]).toBe(ValidationError.NotRepeatedCharacters)
  })

  test('Should pass if all characters are unique', async () => {
    const handler = new NotRepeatedCharacterHandler()
    const success1 = handler.handle(new PasswordValidationChain(''))
    const success2 = handler.handle(new PasswordValidationChain('1234'))
    const success3 = handler.handle(new PasswordValidationChain('ãàa'))
    const success4 = handler.handle(new PasswordValidationChain('abclpPmA'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
  })
})
