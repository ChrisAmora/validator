import { BasePasswordValidationHandler, PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { ValidationError } from '@server/domain'

export class NotRepeatedCharacterHandler extends BasePasswordValidationHandler {
  handle(passwordValidation: PasswordValidationChain): PasswordValidationChain {
    if (passwordValidation.password === '') {
      return super.handle(passwordValidation)
    }
    const passwordMap = new Set(passwordValidation.password)
    if (passwordMap.size < passwordValidation.password.length) {
      passwordValidation.errors.push(ValidationError.NotRepeatedCharacters)
      return passwordValidation
    }

    return super.handle(passwordValidation)
  }
}
