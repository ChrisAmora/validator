import { PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { MinNumberOfSpecialCharactersHandler } from '@server/data/password-validation/min-number-of-special-characters'
import { ValidationError } from '@server/domain'

describe('Min Number Of Special Characters Test', () => {
  test('Should fail if min of special characters is not reached', async () => {
    const handler = new MinNumberOfSpecialCharactersHandler(3)
    const fail1 = handler.handle(new PasswordValidationChain('aa#AA92'))
    const fail2 = handler.handle(new PasswordValidationChain('AMZPQKENM'))
    const fail3 = handler.handle(new PasswordValidationChain(''))
    const fail4 = handler.handle(new PasswordValidationChain('23'))
    const fail5 = handler.handle(new PasswordValidationChain('@EFFEab#'))
    const fail6 = handler.handle(new PasswordValidationChain('♥️♥️A♥️♥️@am'))

    expect(fail1.errors).toHaveLength(1)
    expect(fail1.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
    expect(fail2.errors).toHaveLength(1)
    expect(fail2.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
    expect(fail3.errors).toHaveLength(1)
    expect(fail3.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
    expect(fail4.errors).toHaveLength(1)
    expect(fail4.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
    expect(fail5.errors).toHaveLength(1)
    expect(fail5.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
    expect(fail6.errors).toHaveLength(1)
    expect(fail6.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
  })

  test('Should always pass if min is 0', async () => {
    const handler = new MinNumberOfSpecialCharactersHandler(0)
    const success1 = handler.handle(new PasswordValidationChain('EWFEWF'))
    const success2 = handler.handle(new PasswordValidationChain('1'))
    const success3 = handler.handle(new PasswordValidationChain(''))
    const success4 = handler.handle(new PasswordValidationChain('55'))
    const success5 = handler.handle(new PasswordValidationChain('efwefhuwfehuqooqjei@#$$  feufewuwfe #@!!@000 ♥️'))
    const success6 = handler.handle(new PasswordValidationChain('999999999999'))
    const success7 = handler.handle(new PasswordValidationChain('Á'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
    expect(success5.errors).toHaveLength(0)
    expect(success6.errors).toHaveLength(0)
    expect(success7.errors).toHaveLength(0)
  })

  test('Should pass if min is reached', async () => {
    const handler = new MinNumberOfSpecialCharactersHandler(3)
    const success1 = handler.handle(new PasswordValidationChain('!!##'))
    const success2 = handler.handle(new PasswordValidationChain('♥️%^&M'))
    const success3 = handler.handle(new PasswordValidationChain('34@356-+'))
    const success4 = handler.handle(new PasswordValidationChain('!@!@!@!@!@!@!@!@!@!@'))
    const success5 = handler.handle(new PasswordValidationChain('@@@@aa@@@a'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
    expect(success5.errors).toHaveLength(0)
  })

  test('Should not count special characters that are not listed', async () => {
    const handler = new MinNumberOfSpecialCharactersHandler(1)
    const fail1 = handler.handle(new PasswordValidationChain('♥️♥️♥️'))
    const fail2 = handler.handle(new PasswordValidationChain('~~~~~~~~~'))
    const fail3 = handler.handle(new PasswordValidationChain('``~`````'))
    const fail4 = handler.handle(new PasswordValidationChain('œœœœœœœœœœ'))
    const fail5 = handler.handle(new PasswordValidationChain('ØØØØØØØ'))

    expect(fail1.errors).toHaveLength(1)
    expect(fail1.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
    expect(fail2.errors).toHaveLength(1)
    expect(fail2.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
    expect(fail3.errors).toHaveLength(1)
    expect(fail3.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
    expect(fail4.errors).toHaveLength(1)
    expect(fail4.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
    expect(fail5.errors).toHaveLength(1)
    expect(fail5.errors[0]).toBe(ValidationError.MinNumberOfSpecialCharacters)
  })
})
