import { PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { MinNumberOfDigitsHandler } from '@server/data/password-validation/min-number-of-digits'
import { ValidationError } from '@server/domain'

describe('Min Number Of Digits Test', () => {
  test('Should fail if min of digits is not reached', async () => {
    const handler = new MinNumberOfDigitsHandler(3)
    const fail1 = handler.handle(new PasswordValidationChain('aa#AA92'))
    const fail2 = handler.handle(new PasswordValidationChain('fehuefhu1'))
    const fail3 = handler.handle(new PasswordValidationChain(''))
    const fail4 = handler.handle(new PasswordValidationChain('23'))

    expect(fail1.errors).toHaveLength(1)
    expect(fail1.errors[0]).toBe(ValidationError.MinNumberOfDigits)
    expect(fail2.errors).toHaveLength(1)
    expect(fail2.errors[0]).toBe(ValidationError.MinNumberOfDigits)
    expect(fail3.errors).toHaveLength(1)
    expect(fail3.errors[0]).toBe(ValidationError.MinNumberOfDigits)
    expect(fail4.errors).toHaveLength(1)
    expect(fail4.errors[0]).toBe(ValidationError.MinNumberOfDigits)
  })

  test('Should always pass if min is 0', async () => {
    const handler = new MinNumberOfDigitsHandler(0)
    const success1 = handler.handle(new PasswordValidationChain('aafewfewuhfu'))
    const success2 = handler.handle(new PasswordValidationChain('1'))
    const success3 = handler.handle(new PasswordValidationChain(''))
    const success4 = handler.handle(new PasswordValidationChain('55'))
    const success5 = handler.handle(new PasswordValidationChain('efwefhuwfehuqooqjei@#$$  feufewuwfe #@!!@000 ♥️'))
    const success6 = handler.handle(new PasswordValidationChain('999999999999'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
    expect(success5.errors).toHaveLength(0)
    expect(success6.errors).toHaveLength(0)
  })

  test('Should pass if min is reached', async () => {
    const handler = new MinNumberOfDigitsHandler(3)
    const success1 = handler.handle(new PasswordValidationChain('333'))
    const success2 = handler.handle(new PasswordValidationChain('###4326'))
    const success3 = handler.handle(new PasswordValidationChain('1AAAA2A3'))
    const success4 = handler.handle(new PasswordValidationChain('000'))
    const success5 = handler.handle(new PasswordValidationChain('0#A9999'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
    expect(success5.errors).toHaveLength(0)
  })
})
