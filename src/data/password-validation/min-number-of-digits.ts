import {
  BasePasswordValidationHandler,
  countCharcodeInRange,
  PasswordValidationChain
} from '@server/data/password-validation/base-validator'
import { ValidationError } from '@server/domain'

export class MinNumberOfDigitsHandler extends BasePasswordValidationHandler {
  private readonly minNumberOfDigits: number

  constructor(numberOfDigits: number) {
    super()
    this.minNumberOfDigits = numberOfDigits
  }

  handle(passwordValidation: PasswordValidationChain): PasswordValidationChain {
    const digitsFound = countCharcodeInRange({
      firstCharacter: '0',
      lastCharacter: '9',
      stringToValidate: passwordValidation.password
    })
    if (digitsFound < this.minNumberOfDigits) {
      passwordValidation.errors.push(ValidationError.MinNumberOfDigits)
    }

    return super.handle(passwordValidation)
  }
}
