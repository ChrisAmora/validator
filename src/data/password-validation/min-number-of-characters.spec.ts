import { PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { MinNumberOfCharactersHandler } from '@server/data/password-validation/min-number-of-characters'
import { ValidationError } from '@server/domain'

describe('Min Number Of Characters Test', () => {
  test('Should fail if min of characters is not reached', async () => {
    const handler = new MinNumberOfCharactersHandler(3)
    const fail1 = handler.handle(new PasswordValidationChain('aa'))
    const fail2 = handler.handle(new PasswordValidationChain('@'))
    const fail3 = handler.handle(new PasswordValidationChain(''))
    const fail4 = handler.handle(new PasswordValidationChain('55'))

    expect(fail1.errors).toHaveLength(1)
    expect(fail1.errors[0]).toBe(ValidationError.MinNumberOfCharacters)
    expect(fail2.errors).toHaveLength(1)
    expect(fail2.errors[0]).toBe(ValidationError.MinNumberOfCharacters)
    expect(fail3.errors).toHaveLength(1)
    expect(fail3.errors[0]).toBe(ValidationError.MinNumberOfCharacters)
    expect(fail4.errors).toHaveLength(1)
    expect(fail4.errors[0]).toBe(ValidationError.MinNumberOfCharacters)
  })

  test('Should always pass if min is 0', async () => {
    const handler = new MinNumberOfCharactersHandler(0)
    const success1 = handler.handle(new PasswordValidationChain('aafewfewuhfu'))
    const success2 = handler.handle(new PasswordValidationChain('1'))
    const success3 = handler.handle(new PasswordValidationChain(''))
    const success4 = handler.handle(new PasswordValidationChain('55'))
    const success5 = handler.handle(new PasswordValidationChain('efwefhuwfehuqooqjei@#$$  feufewuwfe #@!!@000 ♥️'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
    expect(success5.errors).toHaveLength(0)
  })

  test('Should pass if min is reached', async () => {
    const handler = new MinNumberOfCharactersHandler(3)
    const success1 = handler.handle(new PasswordValidationChain('aaa'))
    const success2 = handler.handle(new PasswordValidationChain('###'))
    const success3 = handler.handle(new PasswordValidationChain('aaaaaa'))
    const success4 = handler.handle(new PasswordValidationChain('551'))
    const success5 = handler.handle(new PasswordValidationChain('efwefhuwfehuqooqjei@#$$  feufewuwfe #@!!@000 ♥️'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
    expect(success5.errors).toHaveLength(0)
  })
})
