import {
  BasePasswordValidationHandler,
  countCharcodeInRange,
  PasswordValidationChain
} from '@server/data/password-validation/base-validator'
import { ValidationError } from '@server/domain'

export class MinNumberOfUpperCaseLettersHandler extends BasePasswordValidationHandler {
  private readonly minNumberOfUpperCaseLetters: number

  constructor(minNumberOfUpperCaseLetters: number) {
    super()
    this.minNumberOfUpperCaseLetters = minNumberOfUpperCaseLetters
  }

  handle(passwordValidation: PasswordValidationChain): PasswordValidationChain {
    const upperCaseLettersFound = countCharcodeInRange({
      firstCharacter: 'A',
      lastCharacter: 'Z',
      stringToValidate: passwordValidation.password
    })
    if (upperCaseLettersFound < this.minNumberOfUpperCaseLetters) {
      passwordValidation.errors.push(ValidationError.MinNumberOfUpperCaseLetters)
    }

    return super.handle(passwordValidation)
  }
}
