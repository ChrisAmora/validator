import { PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { MinNumberOfLowerCaseLettersHandler } from '@server/data/password-validation/min-number-of-lower-case-letters'
import { ValidationError } from '@server/domain'

describe('Min Number Of Lower Case Letters Test', () => {
  test('Should fail if min of lower case letters is not reached', async () => {
    const handler = new MinNumberOfLowerCaseLettersHandler(3)
    const fail1 = handler.handle(new PasswordValidationChain('aa#AA92'))
    const fail2 = handler.handle(new PasswordValidationChain('AMZPQKENM'))
    const fail3 = handler.handle(new PasswordValidationChain(''))
    const fail4 = handler.handle(new PasswordValidationChain('23'))
    const fail5 = handler.handle(new PasswordValidationChain('EFFEab#'))
    const fail6 = handler.handle(new PasswordValidationChain('###♥️♥️A♥️♥️B♥️55#am##'))

    expect(fail1.errors).toHaveLength(1)
    expect(fail1.errors[0]).toBe(ValidationError.MinNumberOfLowerCaseLetters)
    expect(fail2.errors).toHaveLength(1)
    expect(fail2.errors[0]).toBe(ValidationError.MinNumberOfLowerCaseLetters)
    expect(fail3.errors).toHaveLength(1)
    expect(fail3.errors[0]).toBe(ValidationError.MinNumberOfLowerCaseLetters)
    expect(fail4.errors).toHaveLength(1)
    expect(fail4.errors[0]).toBe(ValidationError.MinNumberOfLowerCaseLetters)
    expect(fail5.errors).toHaveLength(1)
    expect(fail5.errors[0]).toBe(ValidationError.MinNumberOfLowerCaseLetters)
    expect(fail6.errors).toHaveLength(1)
    expect(fail6.errors[0]).toBe(ValidationError.MinNumberOfLowerCaseLetters)
  })

  test('Should always pass if min is 0', async () => {
    const handler = new MinNumberOfLowerCaseLettersHandler(0)
    const success1 = handler.handle(new PasswordValidationChain('EWFEWF'))
    const success2 = handler.handle(new PasswordValidationChain('1'))
    const success3 = handler.handle(new PasswordValidationChain(''))
    const success4 = handler.handle(new PasswordValidationChain('55'))
    const success5 = handler.handle(new PasswordValidationChain('efwefhuwfehuqooqjei@#$$  feufewuwfe #@!!@000 ♥️'))
    const success6 = handler.handle(new PasswordValidationChain('999999999999'))
    const success7 = handler.handle(new PasswordValidationChain('Á'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
    expect(success5.errors).toHaveLength(0)
    expect(success6.errors).toHaveLength(0)
    expect(success7.errors).toHaveLength(0)
  })

  test('Should pass if min is reached', async () => {
    const handler = new MinNumberOfLowerCaseLettersHandler(3)
    const success1 = handler.handle(new PasswordValidationChain('azp'))
    const success2 = handler.handle(new PasswordValidationChain('♥️55A**ZfwefijM'))
    const success3 = handler.handle(new PasswordValidationChain('a1AAcAA2A3b'))
    const success4 = handler.handle(new PasswordValidationChain('KFEOFWEOKaaa'))
    const success5 = handler.handle(new PasswordValidationChain('@@@@aa@@@a'))

    expect(success1.errors).toHaveLength(0)
    expect(success2.errors).toHaveLength(0)
    expect(success3.errors).toHaveLength(0)
    expect(success4.errors).toHaveLength(0)
    expect(success5.errors).toHaveLength(0)
  })
})
