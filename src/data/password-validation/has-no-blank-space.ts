import { BasePasswordValidationHandler, PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { ValidationError } from '@server/domain'

export class HasNoBlankSpaceHandler extends BasePasswordValidationHandler {
  handle(passwordValidation: PasswordValidationChain): PasswordValidationChain {
    const whiteSpaceRegex = /\s+/g

    if (passwordValidation.password.replace(whiteSpaceRegex, '') !== passwordValidation.password) {
      passwordValidation.errors.push(ValidationError.HasNoBlankSpace)
    }

    return super.handle(passwordValidation)
  }
}
