import {
  BasePasswordValidationHandler,
  countCharcodeInRange,
  PasswordValidationChain
} from '@server/data/password-validation/base-validator'
import { ValidationError } from '@server/domain'

export class MinNumberOfLowerCaseLettersHandler extends BasePasswordValidationHandler {
  private readonly minNumberOfLowerCaseLetters: number

  constructor(minNumberOfLowerCaseLetters: number) {
    super()
    this.minNumberOfLowerCaseLetters = minNumberOfLowerCaseLetters
  }

  handle(passwordValidation: PasswordValidationChain): PasswordValidationChain {
    const lowerCaseLettersFound = countCharcodeInRange({
      firstCharacter: 'a',
      lastCharacter: 'z',
      stringToValidate: passwordValidation.password
    })
    if (lowerCaseLettersFound < this.minNumberOfLowerCaseLetters) {
      passwordValidation.errors.push(ValidationError.MinNumberOfLowerCaseLetters)
    }

    return super.handle(passwordValidation)
  }
}
