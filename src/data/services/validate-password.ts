import { PasswordValidationChain } from '@server/data/password-validation/base-validator'
import { HasNoBlankSpaceHandler } from '@server/data/password-validation/has-no-blank-space'
import { MinNumberOfCharactersHandler } from '@server/data/password-validation/min-number-of-characters'
import { MinNumberOfDigitsHandler } from '@server/data/password-validation/min-number-of-digits'
import { MinNumberOfLowerCaseLettersHandler } from '@server/data/password-validation/min-number-of-lower-case-letters'
import { MinNumberOfSpecialCharactersHandler } from '@server/data/password-validation/min-number-of-special-characters'
import { MinNumberOfUpperCaseLettersHandler } from '@server/data/password-validation/min-number-of-upper-case-letters'
import { NotRepeatedCharacterHandler } from '@server/data/password-validation/not-repeated-character'
import { PasswordValidation } from '@server/domain'
import { ValidatePassword } from '@server/domain/usecases'

export interface ValidatePasswordOptions {
  minNumberOfCharacters: number
  minNumberOfDigits: number
  minNumberOfLowerCaseLetters: number
  minNumberOfUpperCaseLetters: number
  minNumberOfSpecialCharacters: number
}

export class ValidatePasswordService implements ValidatePassword {
  constructor(private readonly options: ValidatePasswordOptions) {
    this.options = options
  }

  validate(password: string): PasswordValidation {
    const validation = new PasswordValidationChain(password)
    const handler = new HasNoBlankSpaceHandler()
    handler
      .setNextHandler(new MinNumberOfCharactersHandler(this.options.minNumberOfCharacters))
      .setNextHandler(new MinNumberOfDigitsHandler(this.options.minNumberOfDigits))
      .setNextHandler(new MinNumberOfLowerCaseLettersHandler(this.options.minNumberOfLowerCaseLetters))
      .setNextHandler(new MinNumberOfUpperCaseLettersHandler(this.options.minNumberOfUpperCaseLetters))
      .setNextHandler(new MinNumberOfSpecialCharactersHandler(this.options.minNumberOfSpecialCharacters))
      .setNextHandler(new NotRepeatedCharacterHandler())
    const validatedChain = handler.handle(validation)
    return {
      errors: validatedChain.errors,
      isApproved: validatedChain.errors.length === 0
    }
  }
}
