import { ValidatePasswordOptions, ValidatePasswordService } from '@server/data/services'
import { PasswordValidation, ValidationError } from '@server/domain'
import { defaultValidatePasswordOptions } from '@server/infra/repositories'

interface SutTypes {
  sut: ValidatePasswordService
}

const makeSut = (options: ValidatePasswordOptions): SutTypes => {
  const sut = new ValidatePasswordService(options)
  return {
    sut
  }
}

describe('Validate Password Service', () => {
  test('Should validate a password with default options', async () => {
    const { sut } = makeSut(defaultValidatePasswordOptions)

    const problemDescriptionTests: Record<string, PasswordValidation> = {
      '': {
        isApproved: false,
        errors: [
          ValidationError.MinNumberOfCharacters,
          ValidationError.MinNumberOfDigits,
          ValidationError.MinNumberOfLowerCaseLetters,
          ValidationError.MinNumberOfUpperCaseLetters,
          ValidationError.MinNumberOfSpecialCharacters
        ]
      },
      aa: {
        isApproved: false,
        errors: [
          ValidationError.MinNumberOfCharacters,
          ValidationError.MinNumberOfDigits,
          ValidationError.MinNumberOfUpperCaseLetters,
          ValidationError.MinNumberOfSpecialCharacters,
          ValidationError.NotRepeatedCharacters
        ]
      },
      ab: {
        isApproved: false,
        errors: [
          ValidationError.MinNumberOfCharacters,
          ValidationError.MinNumberOfDigits,
          ValidationError.MinNumberOfUpperCaseLetters,
          ValidationError.MinNumberOfSpecialCharacters
        ]
      },
      AAAbbbCc: {
        isApproved: false,
        errors: [
          ValidationError.MinNumberOfCharacters,
          ValidationError.MinNumberOfDigits,
          ValidationError.MinNumberOfSpecialCharacters,
          ValidationError.NotRepeatedCharacters
        ]
      },
      'AbTp9!foo': {
        isApproved: false,
        errors: [ValidationError.NotRepeatedCharacters]
      },
      'AbTp9!foA': { isApproved: false, errors: [ValidationError.NotRepeatedCharacters] },
      'AbTp9 fok': {
        isApproved: false,
        errors: [ValidationError.HasNoBlankSpace, ValidationError.MinNumberOfSpecialCharacters]
      },
      'AbTp9!fok': { isApproved: true, errors: [] }
    }
    Object.keys(problemDescriptionTests).forEach((password) => {
      expect(sut.validate(password).isApproved).toBe(problemDescriptionTests[password].isApproved)
      expect(sut.validate(password).errors).toEqual(problemDescriptionTests[password].errors)
    })
  })

  test('Should validate a password with other options', async () => {
    const { sut } = makeSut({
      minNumberOfCharacters: 10,
      minNumberOfDigits: 2,
      minNumberOfLowerCaseLetters: 2,
      minNumberOfSpecialCharacters: 2,
      minNumberOfUpperCaseLetters: 4
    })

    const problemDescriptionTests: Record<string, PasswordValidation> = {
      '': {
        isApproved: false,
        errors: [
          ValidationError.MinNumberOfCharacters,
          ValidationError.MinNumberOfDigits,
          ValidationError.MinNumberOfLowerCaseLetters,
          ValidationError.MinNumberOfUpperCaseLetters,
          ValidationError.MinNumberOfSpecialCharacters
        ]
      },
      a: {
        isApproved: false,
        errors: [
          ValidationError.MinNumberOfCharacters,
          ValidationError.MinNumberOfDigits,
          ValidationError.MinNumberOfLowerCaseLetters,
          ValidationError.MinNumberOfUpperCaseLetters,
          ValidationError.MinNumberOfSpecialCharacters
        ]
      },
      ab: {
        isApproved: false,
        errors: [
          ValidationError.MinNumberOfCharacters,
          ValidationError.MinNumberOfDigits,
          ValidationError.MinNumberOfUpperCaseLetters,
          ValidationError.MinNumberOfSpecialCharacters
        ]
      },
      AAAbbbCc: {
        isApproved: false,
        errors: [
          ValidationError.MinNumberOfCharacters,
          ValidationError.MinNumberOfDigits,
          ValidationError.MinNumberOfSpecialCharacters,
          ValidationError.NotRepeatedCharacters
        ]
      },
      'AbTp9!foo': {
        isApproved: false,
        errors: [
          ValidationError.MinNumberOfCharacters,
          ValidationError.MinNumberOfDigits,
          ValidationError.MinNumberOfUpperCaseLetters,
          ValidationError.MinNumberOfSpecialCharacters,
          ValidationError.NotRepeatedCharacters
        ]
      },
      'AbTp9!foA': {
        isApproved: false,
        errors: [
          ValidationError.MinNumberOfCharacters,
          ValidationError.MinNumberOfDigits,
          ValidationError.MinNumberOfUpperCaseLetters,
          ValidationError.MinNumberOfSpecialCharacters,
          ValidationError.NotRepeatedCharacters
        ]
      },
      'AbTp9 fokA@3': {
        isApproved: false,
        errors: [
          ValidationError.HasNoBlankSpace,
          ValidationError.MinNumberOfUpperCaseLetters,
          ValidationError.MinNumberOfSpecialCharacters,
          ValidationError.NotRepeatedCharacters
        ]
      },
      'HAbTp9$fokL@3': { isApproved: true, errors: [] }
    }
    Object.keys(problemDescriptionTests).forEach((password) => {
      expect(sut.validate(password).isApproved).toBe(problemDescriptionTests[password].isApproved)
      expect(sut.validate(password).errors).toEqual(problemDescriptionTests[password].errors)
    })
  })
})
