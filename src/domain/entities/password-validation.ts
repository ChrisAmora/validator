export enum ValidationError {
  MinNumberOfCharacters = 'MinNumberOfCharacters',
  MinNumberOfDigits = 'MinNumberOfDigits',
  MinNumberOfLowerCaseLetters = 'MinNumberOfLowerCaseLetters',
  MinNumberOfUpperCaseLetters = 'MinNumberOfUpperCaseLetters',
  MinNumberOfSpecialCharacters = 'MinNumberOfSpecialCharacters',
  NotRepeatedCharacters = 'NotRepeatedCharacters',
  HasNoBlankSpace = 'HasNoBlankSpace'
}

export interface PasswordValidation {
  isApproved: boolean
  errors: ValidationError[]
}
