import { PasswordValidation } from '@server/domain'

export interface ValidatePassword {
  validate: (password: string) => PasswordValidation
}
